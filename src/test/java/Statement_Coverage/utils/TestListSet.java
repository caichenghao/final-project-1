package Statement_Coverage.utils;
import static org.junit.Assert.*;

import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.junit.*;

import java.util.ArrayList;
import java.util.Collection;

//#1
//gradlew test  gradlew jacocoTestReport
public class TestListSet {
    private FullObject object1;
    private FullObject object2;
    private ListSet list;
    ListSet<String> ls = new ListSet<>();

        ;
    @Test(expected = NullPointerException.class)
    public void testAddNull(){
        Object o =null;
        //object2 = new FullObject(2);
        list.add(o);
    }

    @Test
    public void testAddFalse() {
        ls.add("b");
        Boolean bool=ls.add("b");
        assertEquals(false, bool);
    }

    @Test
    public void testAddTrue() {
        Boolean bool=ls.add("a");
        assertEquals(true, bool);
    }

    @Test
    public void testAddAll() {
        Collection c = new ArrayList();
        c.add("a");
        c.add("b");
        Boolean bool=ls.addAll(c);
        assertEquals(true, bool);
    }

    @Test
    public void testClear(){
        ls.add("b");
        ls.clear();
        assertEquals(0,ls.size());
    }

    @Test(expected = NullPointerException.class)
    public void testContainNull(){
        ls.add("b");
        String s =null;
        ls.contains(s);
    }

    @Test
    public void testContainsTrue(){
        ls.add("b");
        assertEquals(true,ls.contains("b"));
    }

    @Test
    public void testContainsFalse(){
        ls.add("b");
        assertEquals(false,ls.contains("c"));
    }

    @Test
    public void testContainsAllTrue(){
        Collection c1 = new ArrayList();
        c1.add("a");
        c1.add("b");
        ls.add("a");
        ls.add("b");
        assertEquals(true, ls.containsAll(c1));
    }
    @Test
    public void testContainsAllFalse(){
        Collection c2 = new ArrayList();
        c2.add("a");
        c2.add("b");
        c2.add("c");
        Collection c3 = new ArrayList();
        c3.add("a");
        Collection c4 = new ArrayList();
        c4.add("a");
        c4.add("c");

        ls.add("c");
        ls.add("b");
        assertEquals(false, ls.containsAll(c2));
        assertEquals(false,ls.containsAll(c3));
        assertEquals(false,ls.containsAll(c4));
    }
    @Test
    public void testEquals(){
        Collection c = new ArrayList();
        c.add("a");
        c.add("b");
        ls.add("a");
        ls.add("b");
        assertEquals(true, ls.equals(c));
    }
    @Test
    public void testIsEmpty(){
        ls.add("");
        assertEquals(false, ls.isEmpty());
    }
    @Test
    public void testHasNext(){
        ls.add("a");
        ls.add("b");
        ListSet.ListSetIterator it=ls.new ListSetIterator();
        it.remove();
        assertEquals(true, it.hasNext());
    }
    @Test
    public void testNext(){
        ls.add("a");
        ls.add("b");
        ListSet.ListSetIterator it=ls.new ListSetIterator();
        assertEquals("a", it.next());
    }

    @Test
    public void testIterator(){
        ListSet.ListSetIterator it= (ListSet.ListSetIterator) ls.iterator();
        assertTrue(it instanceof  ListSet.ListSetIterator);
    }
    @Test(expected = NullPointerException.class)
    public void testRemoveNull(){
        ls.add("b");
        ls.add("b");
        String s = null;
        ls.remove(s);
    }
    @Test
    public void testRemoveTrue(){
        ls.add("b");
        ls.add("b");
        assertTrue(ls.remove("b"));
    }
    @Test
    public void testRemoveFalse(){
        ls.add("b");
        ls.add("b");
        assertFalse(ls.remove("c"));
    }
    @Test
    public void testRemoveAll(){
        Collection c = new ArrayList();
        c.add("a");
        c.add("b");
        ls.add("a");
        ls.add("b");
        assertTrue(ls.removeAll(c));
    }

    @Test
    public void testRetainAll(){
        Collection c = new ArrayList();
        c.add("a");
        c.add("b");
        ls.add("a");
        ls.add("b");
        ls.add("c");
        ls.retainAll(c);
        assertFalse(ls.contains("c"));
    }
    @Test
    public void testSize(){
        ls.add("a");
        ls.add("b");
        assertEquals(2,ls.size());
    }
    @Test
    public void testToArray(){
        ls.add("a");
        ls.add("b");
        assertTrue(ls.toArray() instanceof Object[]);
    }
    @Test
    public void testToArray2(){
        ls.add("a");
        ls.add("b");
        Object[] a = new Object[]{1,2,3};
        assertTrue(ls.toArray(a) instanceof Object[]);
    }
    @Test
    public void testGetIndexOf(){
        Collection c = new ArrayList();
        c.add("a");
        c.add("b");
        ListSet ls2=new ListSet(c);
        assertEquals(0,ls2.getIndexOf("a"));
        assertEquals(-1,ls2.getIndexOf("c"));
    }
    @Test
    public void testGetElementAt(){
        ls.add("a");
        ls.add("b");
        ls.add("c");
        ls.changeOrder();
        assertEquals("b",ls.getElementAt(0));
        assertEquals("c",ls.getElementAt(1));
        assertEquals("a",ls.getElementAt(2));
    }
    @Test
    public void testToString(){
       // ls.add("");
        assertEquals("{ }\n",ls.toString());
        ls.add("a");
        ls.add("b");
        assertEquals("{ a b }",ls.toString());
    }

}
