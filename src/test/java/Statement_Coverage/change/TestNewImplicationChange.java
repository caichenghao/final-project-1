package Statement_Coverage.change;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import org.junit.Before;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.HashSet;

public class TestNewImplicationChange {
    NewImplicationChange change;
    @Before
    public void runOnceBeforeClass() {

        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        premise.add("b");
        premise.add("c");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("b");
        conclusion.add("c");

        HashSet<String> x = new HashSet<String>();
        x.add("a");
        x.add("b");
        x.add("d");
        x.add("e");
        Implication impl2= new Implication(premise, conclusion);

        FormalContext<String, String> context = new FormalContext<>();
        HashSet<String> attr = new HashSet<String>();
        attr.add("a");
        attr.add("b");
        FullObject object = new FullObject(1,attr);

        try{
            context.addObject(object);
            context.continueExploration(x);
        }catch (Exception e) {
            e.printStackTrace();
        }
        change = new NewImplicationChange(context,impl2);

    }
    @Test(expected = NullPointerException.class)
    public void testUndo(){
        //can not reach since can not add implication to context, followsFromBackgroundKnowledge returns false all the time
        change.undo();
        change.getImplication();

    }
    @Test
    public void testGetImplication(){
        assertEquals("[a, b, c] -> [b, c]",change.getImplication().toString());
    }
    @Test
    public void testGetType(){
        assertEquals(1,change.getType());
    }
}
