package Statement_Coverage.change;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class TestObjectHasAttributeChange {
    ObjectHasAttributeChange change;
    @Before
    public void runOnceBeforeClass() {

        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        premise.add("b");
        premise.add("c");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("b");
        conclusion.add("c");

        HashSet<String> x = new HashSet<String>();
        x.add("a");
        x.add("b");
        x.add("d");
        x.add("e");
        Implication impl2= new Implication(premise, conclusion);

        FormalContext<String, String> context = new FormalContext<>();
        HashSet<String> attr = new HashSet<String>();
        attr.add("a");
        attr.add("b");
        FullObject object = new FullObject(1,attr);

        try{
            context.addObject(object);
            //context.continueExploration(x);
        }catch (Exception e) {
            e.printStackTrace();
        }
        change = new ObjectHasAttributeChange(object,"a");

    }
    @Test
    public void testUndo(){
        change.undo();

    }

    @Test
    public void testGetObject(){
        assertEquals("{id: 1 attributes: [a, b]}",change.getObject().toString());
    }
    @Test
    public void testGetAttribute(){
        assertEquals("a",change.getAttribute().toString());
    }
    @Test
    public void testGetType(){
        assertEquals(0,change.getType());
    }

}
