package Statement_Coverage.action;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.QuestionConfirmedAction;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class TestQuestionConfirmed {
    QuestionConfirmedAction question;
    @Test
    public void testSetQuestion(){
        question=new QuestionConfirmedAction();
        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        conclusion.add("b");
        conclusion.add("c");
        HashSet<String> x = new HashSet<String>();
        x.add("a");
        Implication impl2= new Implication(premise, conclusion);

        question.setQuestion(impl2);
        FormalContext<String, String> context = new FormalContext<>();
        HashSet<String> attr = new HashSet<String>();
        attr.add("a");
        attr.add("b");
        FullObject object = new FullObject(1,attr);

        try{
            context.addObject(object);
        }catch (Exception e) {
            e.printStackTrace();
        }
        context.addAttribute("a");
        context.addAttribute("b");
        context.addAttribute("c");
        //context.continueExploration(conclusion);

        question.setContext(context);

        JButton button= new JButton();
        button.addActionListener(new ActionListener(){
                                      public void actionPerformed(ActionEvent e)
                                      {
                                          question.actionPerformed(e);
                                      }
                                  });
        try{
            button.doClick();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
