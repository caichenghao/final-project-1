package Statement_Coverage.action;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

public class TestCounterExampleProvidedAction {
    CounterExampleProvidedAction counter;
    @Test
    public void testActionPerformed(){

        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        conclusion.add("b");
        conclusion.add("c");
        HashSet<String> x = new HashSet<String>();
        x.add("a");
        Implication impl2= new Implication(premise, conclusion);

        FormalContext<String, String> context = new FormalContext<>();
        HashSet<String> attr = new HashSet<String>();
        attr.add("a");
        attr.add("b");
        FullObject object = new FullObject(1,attr);

        try{
            context.addObject(object);
        }catch (Exception e) {
            e.printStackTrace();
        }
        context.addAttribute("a");
        context.addAttribute("b");
        context.addAttribute("c");
        //context.continueExploration(conclusion);
        counter=new CounterExampleProvidedAction(context,impl2,object);
        //counter.setContext(context);

        JButton button= new JButton();
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                counter.actionPerformed(e);
            }
        });
        try{
            button.doClick();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
