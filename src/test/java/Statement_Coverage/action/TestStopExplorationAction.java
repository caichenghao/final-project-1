package Statement_Coverage.action;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import de.tudresden.inf.tcs.fcalib.action.StopExplorationAction;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

public class TestStopExplorationAction {
    StopExplorationAction stop;
    @Test
    public void testActionPerformed(){
        stop=new StopExplorationAction();

        JButton button= new JButton();
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                stop.actionPerformed(e);
            }
        });
        try{
            button.doClick();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
