package Statement_Coverage.others;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.ImplicationSet;
import org.junit.*;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

//(expected = IllegalObjectException.class)
public class TestImplicationSet {
    ImplicationSet impl;
    @Test
    public void testAddGetContext(){
        FormalContext context = new FormalContext<>();
        context.addAttribute("a");
        impl = new ImplicationSet(context);
        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        Implication impl2= new Implication(premise, conclusion);
        assertTrue(impl.add(impl2));
        assertEquals(context,impl.getContext());
    }
    @Test
    public void testClosureIsClosed(){
        FormalContext context = new FormalContext<>();
        context.addAttribute("a");
        impl = new ImplicationSet(context);
        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        HashSet<String> x = new HashSet<String>();
        x.add("a");

        Implication impl2= new Implication(premise, conclusion);
        Implication impl3= new Implication();
        impl.add(impl3);

        assertEquals("[a]",impl.closure(x).toString());
        assertEquals(true,impl.isClosed(x));
    }
    @Test
    public void testClosure2(){
        FormalContext context = new FormalContext<>();
        context.addAttribute("a");
        context.addAttribute("b");
        context.addAttribute("c");
        impl = new ImplicationSet(context);

        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        HashSet<String> x = new HashSet<String>();
        x.add("a");

        Implication impl2= new Implication(premise, conclusion);
        impl.add(impl2);
        impl.closure(x);
        assertEquals("[a]",impl.closure(x).toString());

    }
    @Test
    public void testNextClosure(){
        HashSet<String> x = new HashSet<String>();
        FormalContext context = new FormalContext<>();

        impl = new ImplicationSet(context);
        x.add("a");
        assertEquals("[]",impl.nextClosure(x).toString());
        context.addAttribute("a");
        assertEquals(null,impl.nextClosure(x));

    }
    @Test
    public void testAllClosures(){
        FormalContext context = new FormalContext<>();
        context.addAttribute("a");
        impl = new ImplicationSet(context);
        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("a");
        HashSet<String> x = new HashSet<String>();
        x.add("a");

        Implication impl2= new Implication(premise, conclusion);
        Implication impl3= new Implication();
        impl.add(impl3);

        assertEquals("[[], [a]]",impl.allClosures().toString());

    }

}
