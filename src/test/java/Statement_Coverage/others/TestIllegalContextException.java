package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestIllegalContextException {
    IllegalContextException ex;
    @Test
    public void testConstructor(){
        ex = new IllegalContextException();
        assertEquals("The base contexts are not matching",ex.getMessage());
    }
    @Test
    public void testConstructor2(){
        ex = new IllegalContextException("Error");
        assertEquals("Error",ex.getMessage());
    }
}
