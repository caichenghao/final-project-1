/**
 * Test implementation for testing attribute exploration in partial contexts.
 * @author Baris Sertkaya
 */
package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.FullObjectDescription;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.StartExplorationAction;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import junit.framework.TestCase;
import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;

/*
 * FCAlib: An open-source extensible library for Formal Concept Analysis 
 *         tool developers
 * Copyright (C) 2009  Baris Sertkaya
 *
 * This file is part of FCAlib.
 * FCAlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCAlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCAlib.  If not, see <http://www.gnu.org/licenses/>.
 */

public class TestFormalContext extends TestCase {
	FormalContext<String, String> context;
	public TestFormalContext() {
	}

	public void testFormalContext() {
		BasicConfigurator.configure();

		FormalContext<String, String> context = new FormalContext<>();
		NoExpertFull<String> expert = new NoExpertFull<>(context);

		context.addAttribute("a");
		context.addAttribute("b");
		context.addAttribute("c");
		// context.addAttribute("d");
		// context.addAttribute("e");
		// context.addAttribute("f");
		// context.addAttribute("g");
		System.out.println("Attributes: " + context.getAttributes());

		expert.addExpertActionListener(context);
		context.setExpert(expert);

		StartExplorationAction<String, String, FullObject<String, String>> action = new StartExplorationAction<>();
		action.setContext(context);
		expert.fireExpertAction(action);
	}
	@Test
	public void testContainsAttribute(){
		boolean x = false;
		boolean y = false;
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject(1,attr);
		try{
			x = context.addObject(object);
		}catch (Exception e){
		};
		try{
			y = context.addObject(object);
		}catch (Exception e){
		};
		assertTrue(x);
		assertFalse(y);
	}
	@Test
	public void testGetObjects(){
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject(1,attr);
		try{
			context.addObject(object);
		}catch (Exception e){
		};
		assertEquals(1,context.getObjects().size());
	}
	@Test
	public void testGetObjectAtIndex(){
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject(1,attr);
		try{
			context.addObject(object);
		}catch (Exception e){
		};
		assertEquals("{id: 1 attributes: [a1, a2]}",context.getObjectAtIndex(0).toString());
	}
	@Test
	public void testGetObject(){
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject("1",attr);
		assertEquals(null,context.getObject("0"));
		try{
			context.addObject(object);
		}catch (Exception e){
		};
		assertEquals("{id: 1 attributes: [a1, a2]}",context.getObject("1").toString());
	}
	@Test (expected = IllegalObjectException.class)
	public void testRemoveObject(){
		boolean x = false;
		context = new FormalContext<>();
		FullObject object = new FullObject("1");

		try{
			context.addObject(object);
			x = context.removeObject("1");
			context.removeObject("2");

		}catch (Exception ex){
		};
		assertTrue(x);
	}
	@Test (expected = IllegalObjectException.class)
	public void testRemoveObject2(){
		boolean x = false;
		context = new FormalContext<>();
//		HashSet<String> attr = new HashSet<String>();
//		attr.add("a1");
//		attr.add("a2");
		FullObject object = new FullObject("1");
		try{
			context.addObject(object);
			x = context.removeObject(object);
			 context.removeObject(object);

		}catch (Exception ex){
		};
		assertTrue(x);
	}
	@Test (expected = IllegalObjectException.class)
	public void testClearObjects(){

		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		try{
			context.addObject(object);

		}catch (Exception ex){
		};
		context.clearObjects();
		assertEquals(null, context.getObject("1"));
	}
	@Test (expected = IllegalObjectException.class)
	public void testAddAttributeToObjectAndAbstractFunctions(){

		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		FullObject object2 = new FullObject("2");
		FullObject object3 = new FullObject("3");
		HashSet<FullObject<String,String>> objects = new HashSet<FullObject<String,String>>();
		objects.add(object2);
		objects.add(object3);
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		context.addAttributes(attr);
		context.addAttribute("a3");
		//already added exception

		assertEquals(3,context.getAttributeCount());


		try{

			context.addObjects(objects);
			context.addObject(object);
			context.addAttributeToObject("a3","1");
			//already added exception
			context.addObject(object);
			context.addAttribute("a3");
			context.addAttribute("a3");

			//null
			context.getNextPremise(context.getImplications().iterator().next().getPremise());

			//can't test protected function
			//context.setCurrentQuestion(impl2);

		}catch (Exception ex){
		};
		assertEquals(3,context.getObjectCount());
		assertEquals("[a3]",context.getObject("1").getDescription().getAttributes().toString());

		assertEquals(null, context.getCurrentQuestion());
		assertEquals(null, context.getImplications());
		context.clearObjects();

	}
	@Test (expected = IllegalObjectException.class)
	public void testAddAttribute2(){

		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		context.addAttributes(attr);
		try{
			context.addObject(object);
			context.addAttributeToObject("a3","1");
		}catch (Exception ex){
		};
		try{
			context.addAttributeToObject("a1","3");
		}catch (Exception ex){
		};
		try{
			context.addAttributeToObject("a1","1");
			context.addAttributeToObject("a1","1");
		}catch (Exception ex){
		};
	}
	@Test (expected = IllegalObjectException.class)
	public void testRemoveAttributeToObjectAndObjectHasAttribute(){
		boolean x = false;
		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		context.addAttribute("a1");

		try{
			context.addObject(object);
			context.addAttributeToObject("a1","1");
			x = context.objectHasAttribute(object,"a1");
			context.removeAttributeFromObject("a1","1");
			//already added exception
			context.addAttribute("a1");

			context.addAttribute("a3");

			//null
			context.getNextPremise(context.getImplications().iterator().next().getPremise());

		}catch (Exception ex){
		};
		assertEquals("[]",context.getObject("1").getDescription().getAttributes().toString());
		assertTrue(x);
		context.clearObjects();
	}
	@Test (expected = IllegalObjectException.class)
	public void testRemoveAttribute2(){

		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		context.addAttributes(attr);
		try{
			context.addObject(object);
			context.removeAttributeFromObject("a3","1");
		}catch (Exception ex){
		};
		try{
			context.removeAttributeFromObject("a1","3");
		}catch (Exception ex){
		};
		try{
			context.removeAttributeFromObject("a1","1");
			context.removeAttributeFromObject("a1","1");
		}catch (Exception ex){
		};
	}

	@Test (expected = IllegalObjectException.class)
	public void testDoublePrimeClosureIsClosed(){
		boolean x = false;
		context = new FormalContext<>();
		FullObject object = new FullObject("1");
		context.addAttribute("a1");
		Set<String> attr = new HashSet<String>();
		attr.add("a1");
		Set<String> temp = new HashSet<String>();
		Set<String> temp2 = new HashSet<String>();
		try{
			context.addObject(object);
			context.addAttributeToObject("a1","1");
		}catch (Exception ex){
		};
		temp =  context.doublePrime(attr);
		temp2=context.closure(attr);
		assertEquals("[a1]",temp.toString());
		assertEquals("[a1]",temp.toString());
		assertTrue(context.isClosed(attr));
	}
	@Test
	public void testNull(){
		context = new FormalContext<>();
		assertEquals(null,context.allClosures());
		assertEquals(null,context.getStemBase());
		assertEquals(null,context.getDuquenneGuiguesBase());
		assertEquals(null,context.getIntents());
		assertEquals(null,context.getExtents());
		assertEquals(null,context.getConcepts());
		assertEquals(null,context.getConceptLattice());
		assertEquals(false,context.followsFromBackgroundKnowledge(new Implication()));
	}

	@Test(expected = IllegalObjectException.class)
	public void testRefutes(){
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject(1,attr);
		Implication impl= new Implication();
		HashSet<String> premise = new HashSet<String>();
		premise.add("a3");

		HashSet<String> conclusion = new HashSet<String>();
		conclusion.add("a3");

		Implication impl2= new Implication(premise, conclusion);

		try{
			context.addObject(object);
		}catch (Exception ex){
		};
		assertEquals(false,context.refutes(impl));
		assertEquals(false,context.refutes(impl2));
	}

	@Test(expected = IllegalObjectException.class)
	public void testIsCounterExampleValid(){
		context = new FormalContext<>();
		HashSet<String> attr = new HashSet<String>();
		attr.add("a1");
		attr.add("a2");
		FullObject object = new FullObject(1,attr);
		Implication impl= new Implication();
		try{
			context.addObject(object);
		}catch (Exception ex){
		};
		assertEquals(false,context.isCounterExampleValid(object,impl));

	}


}
