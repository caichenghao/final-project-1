package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestIllegalObjectException {
    IllegalObjectException ex;

    @Test
    public void testConstructor(){
        ex = new IllegalObjectException("Error");
        assertEquals("Error",ex.getMessage());
    }
}
