package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import org.junit.*;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//#3
public class TestPartialObject {
    private PartialObject object;

    @Test
    public void testRespectsRefutes(){

        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        HashSet<String> neg = new HashSet<String>();
        neg.add("b");
        neg.add("c");
        PartialObject object2 = new PartialObject(1,attr,neg);

        HashSet<String> premise = new HashSet<String>();
        premise.add("a");
        premise.add("b");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("c");
        conclusion.add("d");
        Implication impl2= new Implication();

        assertEquals(true, object2.respects(impl2) );
        assertEquals(false, object2.refutes(impl2) );
    }
    @Test
    public void testGetDescription(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        HashSet<String> neg = new HashSet<String>();
        neg.add("b");
        neg.add("c");
        PartialObject object2 = new PartialObject(1,attr,neg);
        assertEquals("plus=[a1, a2] minus=[b, c]", object2.getDescription().toString());
    }

    @Test
    public void testGetName(){
        object = new PartialObject(1);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        PartialObject object2 = new PartialObject(1,attr);;
        assertEquals("", object.getName() );
    }
    @Test
    public void testSetName(){
        object = new PartialObject(1);
      object.setName("a");
        assertEquals("a", object.getName() );
    }
    @Test
    public void testGetIdentifier(){
        object = new PartialObject(1);
        assertEquals(1, object.getIdentifier() );
    }
    @Test
    public void testToString(){
        object = new PartialObject(1);
        assertEquals("{id=1 plus=[] minus=[]}", object.toString());
    }

}
