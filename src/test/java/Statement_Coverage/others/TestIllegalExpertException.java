package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestIllegalExpertException {
    IllegalExpertException ex;
    @Test
    public void testConstructor(){
        ex = new IllegalExpertException();
        assertEquals("Expert illegal",ex.getMessage());
    }
    @Test
    public void testConstructor2(){
        ex = new IllegalExpertException("name");
        assertEquals("Expert name is illegal",ex.getMessage());
    }
}
