package Statement_Coverage.others;
import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;
import org.junit.*;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class TestPartialObjectDescription {
    private PartialObjectDescription desc;
    @Test
    public void testAddAttribute(){
        desc = new PartialObjectDescription();
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        assertEquals(true,desc.addAttribute(attr) );

    }
    @Test (expected = IllegalArgumentException.class)
    public void testAddAttribute2(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr,attr);
        desc.addAttribute("a1");
    }
    @Test
    public void testAddNegatedAttribute(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr);
        assertEquals(true,desc.addNegatedAttribute(attr) );
        assertEquals(false,desc.addNegatedAttribute(attr) );
    }
    @Test(expected = IllegalArgumentException.class)
    public void testAddNegatedAttribute2(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr,attr);
        desc.addNegatedAttribute("a1");
    }
    @Test
    public void testContainsNegatedAttribute(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr,attr);
        assertEquals(true,desc.containsNegatedAttribute("a1") );
    }
    @Test
    public void testContainsNegatedAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr,attr);
        assertEquals(true,desc.containsNegatedAttributes(attr) );
    }
    @Test
    public void testGetNegatedAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new PartialObjectDescription(attr,attr);
        assertEquals(attr,desc.getNegatedAttributes());
    }
    @Test
    public void testCloneToString(){

        desc = new PartialObjectDescription();
        assertEquals("plus=[] minus=[]",desc.clone().toString());
    }


}
