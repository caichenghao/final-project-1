package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class TestFullObject {
    private FullObject object;
    @Test
    public void testGetSetName() {
        object = new FullObject(1);
        object.setName("a");
        assertEquals("a", object.getName());
    }
    @Test
    public void testRespectsRefutes(){

        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        FullObject object2 = new FullObject(1,attr);

        Implication impl= new Implication();

        assertEquals(true, object2.respects(impl) );
        assertEquals(false, object2.refutes(impl) );
    }
    @Test
    public void testGetDescription() {
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        FullObject object = new FullObject(1,attr);
        assertEquals(true, object.getDescription().containsAttributes(attr));
    }
    @Test
    public void testGetIdentifier() {
        object = new FullObject(1);
        assertEquals(1, object.getIdentifier());
    }
    @Test
    public void testToString() {
        object = new FullObject(1);
        assertEquals("{id: 1 attributes: []}", object.toString());
    }
}
