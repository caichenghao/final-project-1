package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.*;

import java.util.HashSet;


import static org.junit.Assert.*;
//#2
public class TestImplication {
    private Implication impl = new Implication();

    @Test
    public void testGetConclusion(){
        assertTrue(impl.getConclusion() instanceof HashSet);
    }

    @Test
    public void testGetPremise(){
        HashSet<String> premise = new HashSet<String>();
        premise.add("p1");
        premise.add("p2");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("p1");
        conclusion.add("p2");
        Implication impl2= new Implication(premise, conclusion);
        assertEquals(premise, impl2.getPremise() );
    }
    @Test
    public void testEquals(){
        HashSet<String> premise = new HashSet<String>();
        premise.add("p1");
        premise.add("p2");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("p1");
        conclusion.add("p2");
        Implication impl2= new Implication(premise, conclusion);
        Implication impl3= new Implication(premise, conclusion);

        HashSet<String> premise2 = new HashSet<String>();
        premise.add("p2");
        premise.add("p3");
        HashSet<String> conclusion2 = new HashSet<String>();
        conclusion.add("p2");
        conclusion.add("p3");
        Implication impl4= new Implication(premise, conclusion2);
        Implication impl5= new Implication(premise2, conclusion);

        assertTrue(impl3.equals(impl2));
        assertFalse(impl3.equals(impl));
        assertFalse(impl3.equals(impl4));
        assertFalse(impl3.equals(impl5));
    }
    @Test
    public void testToString(){
        HashSet<String> premise = new HashSet<String>();
        premise.add("p1");
        premise.add("p2");
        HashSet<String> conclusion = new HashSet<String>();
        conclusion.add("p1");
        conclusion.add("p2");
        Implication impl2= new Implication(premise, conclusion);

        assertEquals("[p1, p2] -> [p1, p2]",impl2.toString());
    }
}
