package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestIllegalAttributeException {
    IllegalAttributeException ex;
    @Test
    public void testConstructor(){
        ex = new IllegalAttributeException();
        assertEquals(null,ex.getMessage());
    }
    @Test
    public void testConstructor2(){
        ex = new IllegalAttributeException("Error");
        assertEquals("Error",ex.getMessage());
    }
}
