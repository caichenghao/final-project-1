package Statement_Coverage.others;

import de.tudresden.inf.tcs.fcalib.FullObjectDescription;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestFullObjectDescription {
    private FullObjectDescription desc;
    @Test
    public void testContainsAttribute(){
        desc = new FullObjectDescription();
        //assertEquals(true,desc.addAttribute(attr) );
        assertFalse(desc.containsAttribute("a"));
    }
    @Test
    public void testContainsAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new FullObjectDescription(attr);
        assertTrue(desc.containsAttributes(attr));
    }
    @Test
    public void testAddAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new FullObjectDescription();
        assertTrue(desc.addAttributes(attr));
    }
    @Test
    public void testRemoveAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new FullObjectDescription(attr);
        assertTrue(desc.removeAttribute("a1"));
    }
    @Test
    public void testGetAttributes(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        desc = new FullObjectDescription(attr);
        assertEquals(attr,desc.getAttributes());
    }
    @Test
    public void testCloneToString() throws CloneNotSupportedException {

        desc = new FullObjectDescription();
        assertTrue(desc.clone() instanceof FullObjectDescription);
    }

}
