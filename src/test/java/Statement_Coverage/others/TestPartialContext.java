package Statement_Coverage.others;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPartialContext {

    PartialObject object2 = new PartialObject(1);
    PartialContext context;

    @Test
    public void testAddAndGetObjectAtIndexCount(){
        boolean x ;
        boolean y;
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        HashSet<String> neg = new HashSet<String>();
        neg.add("b");
        neg.add("c");
        PartialObject object2 = new PartialObject(1,attr,neg);
        context=new PartialContext();
        assertEquals(null,context.getObject("0"));
        x = context.addObject(object2);
        y = context.addObject(object2);
        assertTrue(x);
        assertFalse(y);
        assertEquals(object2,context.getObject(1));
        assertEquals(object2,context.getObjectAtIndex(0));
        assertEquals(1,context.getObjectCount());

    }
    @Test
    public void testRemoveObjectId() {
        boolean z= false;
        PartialObject object2 = new PartialObject(1);
        context=new PartialContext();
        context.addObject(object2);
         try {
             z = context.removeObject(1);
            assertEquals(true,z);
        } catch (IllegalObjectException e) {
            e.printStackTrace();
        }
    //removes don't return false
    }
    @Test
    public void testRemoveObject() {
        boolean z= false;
        PartialObject object2 = new PartialObject(1);
        context=new PartialContext();
        context.addObject(object2);
        try {
            z = context.removeObject(object2);
            assertEquals(true,z);
        } catch (IllegalObjectException e) {
            e.printStackTrace();
        }
        //removes don't return false
    }
    @Test
    public void testRespectsRefutesAndIsCounterExampleValid(){
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        context=new PartialContext();
        context.addObject(object2);
        Implication impl= new Implication();
        assertEquals(false, context.refutes(impl) );
        assertEquals(false, context.isCounterExampleValid(object2,impl) );
    }
    @Test
    public void testClearObject() {
        boolean z= false;
        PartialObject object2 = new PartialObject(1);
        context=new PartialContext();
        context.addObject(object2);
        context.clearObjects();
        assertEquals(0,context.getObjectCount());

    }
    @Test
    public void testAddAttributeToObject(){
        boolean x =false;
        boolean y =false;
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        context=new PartialContext();
        context.addObject(object2);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        context.addAttributes(attr);
        try{
            context.addObject(object2);
            x=context.addAttributeToObject("a1",1);
        }catch (Exception ex){
        };
        assertEquals(true, x);

    }
    //
    @Test
    public void testAddAttribute2(){
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        context.addAttributes(attr);
        try{
            context.addObject(object2);
            context.addAttributeToObject("a3",1);
        }catch (Exception ex){
        };
        try{
            context.addAttributeToObject("a1",3);
        }catch (Exception ex){
        };
        try{
            context.addAttributeToObject("a1",1);
            context.addAttributeToObject("a1",1);
        }catch (Exception ex){
        };
    }
    @Test
    public void testRemoveAttribute(){
        boolean x =false;
        boolean y =false;
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        context=new PartialContext();
        context.addObject(object2);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        context.addAttributes(attr);
        try{
            context.addObject(object2);
            context.addAttributeToObject("a1",1);
            x=context.removeAttributeFromObject("a1",1);
        }catch (Exception ex){
        };
        assertEquals(true, x);

    }
    @Test
    public void testRemoveAttribute2(){
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        context.addAttributes(attr);
        try{
            context.addObject(object2);
            context.removeAttributeFromObject("a3",1);
        }catch (Exception ex){
        };
        try{
            context.removeAttributeFromObject("a1",3);
        }catch (Exception ex){
        };
        try{
            context.addAttributeToObject("a1",1);
            context.removeAttributeFromObject("a1",1);
            context.removeAttributeFromObject("a1",1);
        }catch (Exception ex){
        };
    }
    @Test
    public void testObjectHasAttributeNegative(){
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        HashSet<String> attr = new HashSet<String>();
        attr.add("a1");
        attr.add("a2");
        assertFalse(context.objectHasAttribute(object2,attr));
        assertFalse(context.objectHasNegatedAttribute(object2,attr));
    }
    @Test
    public void testDoublePrimeClosureIsClosed(){
        boolean x = false;
        context = new PartialContext();
        PartialObject object2 = new PartialObject(1);
        context.addAttribute("a1");
        Set<String> attr = new HashSet<String>();
        attr.add("a1");
        Set<String> temp = new HashSet<String>();
        Set<String> temp2 = new HashSet<String>();
        try{
            context.addObject(object2);
            context.addAttributeToObject("a1","1");
        }catch (Exception ex){
        };
        temp =  context.doublePrime(attr);

        assertEquals("[a1]",temp.toString());


    }
    @Test
    public void testGetStemBase(){
        context = new PartialContext();
        assertEquals(null,context.getStemBase());
    }
    @Test
    public void testGetDuquenneGuiguesBase(){
        context = new PartialContext();
        assertEquals(null,context.getDuquenneGuiguesBase());
    }

}
