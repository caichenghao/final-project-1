package isp_test.interface_test.change;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class ObjectHasAttributeChangeTest {

    FormalContext<String, String> context = new FormalContext<>();
    HashSet<String> attr = new HashSet<String>();
    FullObject object = new FullObject(1,attr);

    ObjectHasAttributeChange obj = new ObjectHasAttributeChange(object,"a");
    @Test
    public void undo() {
    }

    @Test
    public void getObject() {
    }

    @Test
    public void getAttribute() {
    }

    @Test
    public void getType() {
    }
}