package isp_test.interface_test.change;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class NewObjectChangeTest {

    FormalContext<String, String> context = new FormalContext<>();
    HashSet<String> attr = new HashSet<String>();
    FullObject object = new FullObject(1,attr);
    NewObjectChange change = new NewObjectChange(context,object);

    @Test
    public void undo() {

    }

    @Test
    public void getObject() {
    }

    @Test
    public void getType() {
    }
}