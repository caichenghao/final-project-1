package isp_test.interface_test.change;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class NewImplicationChangeTest {
    NewImplicationChange change;
    @Before
    public void init(){
        Set<String> premise = new HashSet<>();
        premise.add("a");
        premise.add("b");
        premise.add("c");
        Set<String> conclusion = new HashSet<>();
        conclusion.add("b");
        conclusion.add("c");
        Implication imp= new Implication(premise, conclusion);

        Set<String> x = new HashSet<>();
        x.add("a");
        x.add("b");
        x.add("d");
        x.add("e");

        Set<String> attr = new HashSet<>();
        attr.add("a");
        attr.add("b");
        FullObject object = new FullObject(1,attr);

        FormalContext<String, String> context = new FormalContext<>();
        try{
            context.addObject(object);
            context.continueExploration(x);
        }catch (Exception e) {
            e.printStackTrace();
        }
        change = new NewImplicationChange(context,imp);
    }

    @Test
    public void getImplication() {
        change.getImplication();
    }

    @Test
    public void getType() {
        change.getType();
    }
}