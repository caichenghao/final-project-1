package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class PartialObjectTest {

    Set<String> attrs = new HashSet<>(){{add("a");}};
    Set<String> negate = new HashSet<>(){{add("b");}};
    public PartialObject<String , Integer> obj1 = new PartialObject<>(1);
    public PartialObject<String , Integer> obj2 = new PartialObject<>(1,attrs);
    public PartialObject<String , Integer> obj3 = new PartialObject<>(1,attrs,negate);


    @Test
    public void respects() {
        Set<String> p = new HashSet<>(){{add("a");}};
        Set<String> c = new HashSet<>(){{add("b");}};
        Implication<String> imp = new Implication<>(p,c);
        obj1.respects(imp);
        obj2.respects(imp);
        obj3.respects(imp);
    }

    @Test
    public void refutes() {
        Set<String> p = new HashSet<>(){{add("a");}};
        Set<String> c = new HashSet<>(){{add("b");}};
        Implication<String> imp = new Implication<>(p,c);
        obj1.refutes(imp);
        obj2.refutes(imp);
        obj3.refutes(imp);
    }

    @Test
    public void setName() {
        String n1 = "";
        String n2 = "a";

        obj1.setName(n1);
        obj2.setName(n2);
    }
}