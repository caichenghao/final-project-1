package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class FullObjectTest {

    public FullObject<String, Integer> obj1 = new FullObject<>(1);
    Set<String> attrs = new HashSet<>();
    public FullObject<String, Integer> obj2 = new FullObject<>(2,attrs);

    @Test
    public void setName() {
        String n1 = "";
        String n2 = "abc";
        obj1.setName(n1);
        obj1.setName(n2);
        obj2.setName(n1);
        obj2.setName(n2);
    }

    @Test
    public void respects() {
        Set<String> p = new HashSet<>(); // premise
        Set<String> c = new HashSet<>(); //conclusion
        Implication<String> imp = new Implication<>(p,c);
        obj1.respects(imp);
        obj1.refutes(imp);
    }

//    @Test
//    public void others() {
//        obj1.getName();
//        obj1.toString();
//        obj1.getIdentifier();
//    }
}