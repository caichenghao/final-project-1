package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ImplicationTest {

    Set<String> premise = new HashSet<>();
    Set<String> conclusion = new HashSet<>();
    public Implication<String> imp = new Implication<>(premise,conclusion);

    @Test
    public void getConclusion() {
        imp.getConclusion();
    }

    @Test
    public void getPremise() {
        imp.getPremise();
    }

    @Test
    public void equals() {
        Implication<String> tmp1 = new Implication<>();
        imp.equals(tmp1);
        Set<String> p = new HashSet<>();
        Set<String> c = new HashSet<>();

        p.add("a"); c.add("a");// a->a
        Implication<String> tmp2 = new Implication<>(p,c);
        imp.equals(tmp2);

        p.add("b");// ab->a
        Implication<String> tmp3 = new Implication<>(p,c);
        imp.equals(tmp3);

        //a->ab
        Implication<String> tmp4 = new Implication<>(c,p);
        imp.equals(tmp4);

        //a->b
        Set<String> b = new HashSet<String>(){{add("b");}};
        Implication<String> tmp5 = new Implication<>(c,b);
        imp.equals(tmp5);

    }

    @Test
    public void toStringTest() {
        imp.toString();
    }
}