package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class PartialObjectDescriptionTest {

    Set<String> negate = new HashSet<>();
    Set<String> posit = new HashSet<>();

    public PartialObjectDescription<String> obj1 = new PartialObjectDescription<>();
    public PartialObjectDescription<String> obj2 = new PartialObjectDescription<>(posit);
    public PartialObjectDescription<String> obj3 = new PartialObjectDescription<>(posit,negate);
    @Test
    public void addAttribute() {
        obj1.addAttribute("");
        obj2.addAttribute("a");
    }

    @Test
    public void addNegatedAttribute() {
        obj1.addAttribute("");
        obj2.addAttribute("a");
    }

    @Test
    public void containsNegatedAttribute() {
        obj1.containsNegatedAttribute("");
        obj2.containsNegatedAttribute("a");
    }

    @Test
    public void containsNegatedAttributes() {
        Set<String> attrs = new HashSet<>();
        obj1.containsNegatedAttributes(attrs);
        attrs.add("a");
        obj2.containsNegatedAttributes(attrs);
    }
}