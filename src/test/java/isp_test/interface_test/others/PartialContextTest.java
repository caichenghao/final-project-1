package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class PartialContextTest {

    PartialContext<String, Integer,PartialObject<String,Integer>> pc1 = new PartialContext();
    PartialContext<String, Integer,PartialObject<String,Integer>> pc2= new PartialContext();
    PartialObject<String,Integer> obj = new PartialObject<String,Integer>(1);
    Set<String> attrs = new HashSet<>();
    Set<String> p = new HashSet<>();
    Set<String> c = new HashSet<>();
    Implication<String> imp = new Implication<>(p,c);

//    @Test
//    public void getObjectAtIndex() {
//        pc1.getObjectAtIndex(-1);
//        pc1.getObjectAtIndex(0);
//        pc1.getObjectAtIndex(1);
//
//        pc1.getObject(1);
//    }

//    @Test
//    public void removeObject() {
//        try {
//            pc1.removeObject(1);
//            //pc1.removeObject(0);
//        } catch (IllegalObjectException e) {
//            e.printStackTrace();
//        }
//    }


    @Test
    public void refutes() {
        pc1.refutes(imp);
    }

    @Test
    public void isCounterExampleValid() {
        pc1.isCounterExampleValid(obj,imp);
    }

    @Test
    public void addObject() {

        PartialObject<String,Integer> obj1 = null;
        pc1.addObject(obj);
        //pc1.addObject(obj1);
    }

//    @Test
//    public void addAttributeToObject() {
//        try {
//            pc1.addAttributeToObject("b",1);
//        } catch (IllegalObjectException e) {
//            e.printStackTrace();
//        }
//    }

//    @Test
//    public void removeAttributeFromObject() throws IllegalObjectException {
//        pc1.removeAttributeFromObject("b",1);
//    }

    @Test
    public void objectHasAttribute() {
        pc1.objectHasAttribute(obj,"a");
    }

    @Test
    public void objectHasNegatedAttribute() {
        pc1.objectHasNegatedAttribute(obj,"b");
    }

    @Test
    public void doublePrime() {
        pc1.doublePrime(attrs);
        attrs.add("a");
        pc1.doublePrime(attrs);
    }

    @Test
    public void followsFromBackgroundKnowledge() {
    }
}