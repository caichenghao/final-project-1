package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcalib.FullObjectDescription;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class FullObjectDescriptionTest {

    public FullObjectDescription<String> description = new FullObjectDescription<>();

    @Test
    public void containsAttribute() {
        description.containsAttribute("");
        description.containsAttribute("a");
    }

    @Test
    public void containsAttributes() {
        Set<String> atts= new HashSet<>();
        description.containsAttributes(atts);
        atts.add("a");
        atts.add("b");
        description.containsAttributes(atts);
    }

    @Test
    public void addAttribute() {
        description.addAttribute("");
        description.addAttribute("a");
    }

    @Test
    public void addAttributes() {
        Set<String> atts= new HashSet<>();
        description.addAttributes(atts);
        atts.add("a");
        atts.add("b");
        description.addAttributes(atts);
    }

    @Test
    public void removeAttribute() {
        description.removeAttribute("");
        description.removeAttribute("a");
    }

    @Test
    public void noParaTest(){
        description.getAttributes();
        try {
            description.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}