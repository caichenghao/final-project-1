package isp_test.interface_test.others;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.ImplicationSet;
import org.junit.Test;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ImplicationSetTest {

    FormalContext context = new FormalContext<>(){{addAttribute("a");}};

    ImplicationSet ips = new ImplicationSet(context);

    Set<String> p = new HashSet<String>(){{add("a");}};
    Set<String> c = new HashSet<String>(){{add("a");}};

    Implication imp= new Implication(p,c);

    @Test
    public void add() {
        ips.add(imp);

    }

    @Test
    public void closure() {
        Set<String> x = new HashSet<>();
        ips.closure(x);
        x.add("b");
        //ips.closure(x);
    }

    @Test
    public void isClosed() {
        Set<String> x = new HashSet<>();
        ips.isClosed(x);
        x.add("b");
        //ips.isClosed(x);
    }

    @Test
    public void nextClosure() {
        Set<String> x = new HashSet<>();
        ips.nextClosure(x);
        x.add("a");
        ips.nextClosure(x);
    }
}