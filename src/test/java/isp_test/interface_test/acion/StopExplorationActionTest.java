package isp_test.interface_test.acion;

import de.tudresden.inf.tcs.fcalib.action.StopExplorationAction;
import javafx.scene.paint.Stop;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.junit.Assert.*;

public class StopExplorationActionTest {

    public StopExplorationAction act = new StopExplorationAction();
    @Test
    public void actionPerformed() {

        JButton button = new JButton();
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        act.actionPerformed(e);
                    }
                }
        );
        //execute twice, first time comment out this action
        // second time use this action.
        try{
            button.doClick();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void noActionPerformed() {

        JButton button = new JButton();
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        act.actionPerformed(e);
                    }
                }
        );
        //execute twice, first time comment out this action
        // second time use this action.
//        try{
//            button.doClick();
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}