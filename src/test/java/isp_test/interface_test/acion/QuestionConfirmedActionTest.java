package isp_test.interface_test.acion;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.QuestionConfirmedAction;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class QuestionConfirmedActionTest {

    public QuestionConfirmedAction act = new QuestionConfirmedAction();

    @Test
    public void actionPerformed() {

        JButton button = new JButton();
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        act.actionPerformed(e);
                    }
                }
        );
        //execute twice, first time comment out this action
        // second time use this action.
        try{
            button.doClick();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void noActionPerformed() {

        JButton button = new JButton();
        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        act.actionPerformed(e);
                    }
                }
        );
        //execute twice, first time comment out this action
        // second time use this action.
//        try{
//            button.doClick();
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}