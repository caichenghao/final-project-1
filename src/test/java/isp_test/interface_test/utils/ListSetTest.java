package isp_test.interface_test.utils;

import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class ListSetTest {

    public ListSet<String> ls = new ListSet<>();
    public String addend;
    public ArrayList<String> adds = new ArrayList<>();

    @Before
    public void init(){
        ls.add("a");
        ls.add("b");
    }
    /**
     * characteristics: empty or non-empty string addition
     */
    @Test
    public void addEmpty() {
        addend="";
        ls.add(addend);
        ls.addAll(adds);
    }
    @Test
    public void addNonEmpty() {
        addend="any string";
        adds.add("abc");
        ls.add(addend);
        ls.addAll(adds);
    }
    /**
     * characteristics: null or not null string addition
     */
//    @Test
//    public void addNull() {
//        addend=null;
//        adds=null;
//        ls.add(addend);
//        ls.addAll(adds);
//    }
    @Test
    public void addNotNull() {
        addend="not null";
        ls.add(addend);
        adds.add("abc");
        ls.addAll(adds);
    }

    /**
     * characteristics: contain null or not null string
     */
//    @Test
//    public void containsNull() {
//        addend=null;
//        adds=null;
//        ls.contains(addend);
//        ls.containsAll(adds);
//    }
    @Test
    public void containsNotNull() {
        addend="a";
        adds.add("a");
        adds.add("b");
        ls.contains(addend);
        ls.containsAll(adds);
    }

    @Test
    public void equalsWithSize() {
        addend="ab";
        ls.equals(addend);
        addend="abcd";
        ls.equals(addend);
    }
    /**
     * characteristics: empty or non-empty string addition
     */
    @Test
    public void removeEmpty() {
        addend="";
        ls.remove(addend);
        ls.removeAll(adds);
    }
    @Test
    public void removeNonEmpty() {
        addend="any string";
        adds.add("abc");
        ls.remove(addend);
        ls.removeAll(adds);
    }

    @Test
    public void retainAllTest() {
        adds.add("");
        ls.retainAll(adds);
        adds.add("ab");
        ls.retainAll(adds);
    }

    /**
     *  Characteristics: whether the array size equals to 0
     */
    @Test
    public void toArrayTest() {

    }

    @Test
    public void getIndexOfTest() {

        ls.getIndexOf("");
        ls.getIndexOf("a");

    }

    @Test
    public void getElementAt() {
//        ls.getElementAt(-1);
//        ls.getElementAt(0);
        ls.getElementAt(1);
    }
}