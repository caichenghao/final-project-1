import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.*;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Main {
    public static void main(String args[]) {
        ImplicationSet impl;
        HashSet<String> x = new HashSet<String>();
        FormalContext context = new FormalContext<>();

        impl = new ImplicationSet(context);
        x.add("a");

        context.addAttribute("a");
        context.addAttribute("b");
        context.addAttribute("c");
        context.addAttribute("d");
        impl.nextClosure(x).toString();
    }
}
